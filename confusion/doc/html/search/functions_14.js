var searchData=
[
  ['vectordistance',['VectorDistance',['../namespaceconfusion.html#a75c352ff0fa4cea4a725b2b3a07de8f7',1,'confusion']]],
  ['vectortorosmsg',['VectorToRosMsg',['../namespaceconfusion.html#a67e1e76455df920b78e545689dae16da',1,'confusion::VectorToRosMsg(const Eigen::Vector3d &amp;vector)'],['../namespaceconfusion.html#a0b922480ec9791f28cd66ac1b0b5d495',1,'confusion::vectorToRosMsg(std::string topic, Eigen::VectorXd vec, double stamp, std::string label=&quot;&quot;)']]],
  ['verboseprint',['verbosePrint',['../classconfusion_1_1ConFusor.html#a1bd1e5c58d9de5c28141d47ceae06f6e',1,'confusion::ConFusor']]],
  ['vsprintf_5falloc',['vsprintf_alloc',['../string__util_8c.html#a87423f0debcdeec363d63eb947c15f3a',1,'vsprintf_alloc(const char *fmt, va_list orig_args):&#160;string_util.c'],['../string__util_8h.html#a51c1270a584246dadd7471b455bde4ae',1,'vsprintf_alloc(const char *fmt, va_list args):&#160;string_util.c']]]
];
