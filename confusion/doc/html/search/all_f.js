var searchData=
[
  ['overview',['Overview',['../index.html',1,'']]],
  ['odometry_5fstddev_5f',['odometry_stddev_',['../structTagTrackerParameters.html#a6432b70951a89450da444b067d817b71',1,'TagTrackerParameters']]],
  ['on',['on',['../plotConFusionData_8m.html#a58ab1fd68e97078232808206b850161b',1,'plotConFusionData.m']]],
  ['opencv_5fdemo_2ecc',['opencv_demo.cc',['../opencv__demo_8cc.html',1,'']]],
  ['operator_28_29',['operator()',['../classconfusion_1_1PoseCost.html#adfc3ade0ac0abfd2fbd2ef1214f1891e',1,'confusion::PoseCost::operator()()'],['../classconfusion_1_1TagCost.html#a3b669db56fd71e196efb44c43578768f',1,'confusion::TagCost::operator()()'],['../classconfusion_1_1TrajectoryAlignmentCost.html#a5c99f63b9718d14ed69ffdf754f1a1b1',1,'confusion::TrajectoryAlignmentCost::operator()()'],['../classconfusion_1_1AlignTrajCallback.html#a3aa037e01e6cd79c6ec633326aa00b6c',1,'confusion::AlignTrajCallback::operator()()'],['../classconfusion_1_1SolverOverrunCallback.html#aca8b1f1ea6db067b23782e4b1bc1a840',1,'confusion::SolverOverrunCallback::operator()()']]],
  ['operator_2a',['operator*',['../classconfusion_1_1Pose.html#af4f86d21321bca38e169a1db6f3d21b1',1,'confusion::Pose::operator*(const Pose&lt; T &gt; &amp;p_in) const'],['../classconfusion_1_1Pose.html#a5d637bd4c4e9e5359fb642ee77731c09',1,'confusion::Pose::operator*(const Eigen::Matrix&lt; T, 3, 1 &gt; &amp;p_in) const']]],
  ['operator_3d',['operator=',['../classconfusion_1_1ImuMeas.html#ab82a75fdf1586287da9e03bee69c804d',1,'confusion::ImuMeas::operator=()'],['../classconfusion_1_1StaticParameterVector.html#afb9b9ebd8d85df0496b41ee002f60355',1,'confusion::StaticParameterVector::operator=()'],['../classconfusion_1_1Pose.html#a02c5dafd5fbb7f73f3a5b1fbdd0023f9',1,'confusion::Pose::operator=()']]],
  ['operator_5b_5d',['operator[]',['../classconfusion_1_1StateVector.html#aec692ed5da2ef54344253477b4826afe',1,'confusion::StateVector::operator[](size_t pos)'],['../classconfusion_1_1StateVector.html#ae7f38ddfc8df132f078c8b63941ba8cd',1,'confusion::StateVector::operator[](size_t pos) const']]],
  ['optgravity_5f',['optGravity_',['../classconfusion_1_1ImuChain.html#aa2c03d4fbc5466d89af567a691706795',1,'confusion::ImuChain']]],
  ['optimize',['optimize',['../classconfusion_1_1BatchFusor.html#a1328b167dfebce586d8f3673ca6dc3f5',1,'confusion::BatchFusor::optimize()'],['../classconfusion_1_1ConFusor.html#aa84a6c0fb5c499f551999e6235ce788c',1,'confusion::ConFusor::optimize()']]],
  ['optimize_5fquad_5fgeneric',['optimize_quad_generic',['../apriltag_8c.html#a299aca06d9cfd075d43e6c926024df24',1,'apriltag.c']]],
  ['options',['options',['../structgetopt.html#aa28a9eb8f7390bd5d4bc6cddb9bca5e0',1,'getopt']]],
  ['out',['out',['../structzmaxheap__iterator.html#a064b37f345778fe54d5fe44d9ecb944d',1,'zmaxheap_iterator::out()'],['../readLogFileLine_8m.html#a871a608f9a44fe73070b772b7766719a',1,'out():&#160;readLogFileLine.m'],['../readLogFileLine_8m.html#a2d3585954be12625e6942d14aeb1318a',1,'out(end+1, 1:length(currentData)):&#160;readLogFileLine.m'],['../readLogFileLine_8m.html#aacfb5a1dcddba5853590ca3972db64f6',1,'out(end, length(currentData)+1:end):&#160;readLogFileLine.m']]]
];
