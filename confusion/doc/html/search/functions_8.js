var searchData=
[
  ['homography_5fcompute',['homography_compute',['../homography_8c.html#a3c46efdf831cf84e447a3e46cef773fe',1,'homography_compute(zarray_t *correspondences, int flags):&#160;homography.c'],['../homography_8h.html#a3c46efdf831cf84e447a3e46cef773fe',1,'homography_compute(zarray_t *correspondences, int flags):&#160;homography.c']]],
  ['homography_5fto_5fmodel_5fview',['homography_to_model_view',['../homography_8c.html#acf33409d37c2616abc41b0ff0c8a83ba',1,'homography_to_model_view(const matd_t *H, double F, double G, double A, double B, double C, double D):&#160;homography.c'],['../homography_8h.html#acf33409d37c2616abc41b0ff0c8a83ba',1,'homography_to_model_view(const matd_t *H, double F, double G, double A, double B, double C, double D):&#160;homography.c']]],
  ['homography_5fto_5fpose',['homography_to_pose',['../homography_8c.html#a9c10d8cf51b40a281f842a08ade82beb',1,'homography_to_pose(const matd_t *H, double fx, double fy, double cx, double cy):&#160;homography.c'],['../homography_8h.html#a9c10d8cf51b40a281f842a08ade82beb',1,'homography_to_pose(const matd_t *H, double fx, double fy, double cx, double cy):&#160;homography.c']]]
];
