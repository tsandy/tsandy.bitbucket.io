var searchData=
[
  ['b',['B',['../structgraymodel.html#a696897f0eed52996bbfb102aae69bff4',1,'graymodel']]],
  ['ba_5finit_5fstddev',['ba_init_stddev',['../structTagTrackerParameters.html#ac7225effb9f459f721c2a79bb7b0fedb',1,'TagTrackerParameters']]],
  ['ba_5fstddev_5f',['ba_stddev_',['../structTagTrackerParameters.html#a5c945fbe8706c2f5030d385649c48006',1,'TagTrackerParameters::ba_stddev_()'],['../ImuChainTest_8cpp.html#a362ae6f15c276bbc6753d4c556280bf6',1,'ba_stddev_():&#160;ImuChainTest.cpp']]],
  ['bag_5fname',['bag_name',['../plotConFusionData_8m.html#aba2b331d3f864f02c69435c7ee9d7681',1,'plotConFusionData.m']]],
  ['bagname',['bagName',['../importConFusionData_8m.html#ad945a163fa00c66fa019509ef4308fd4',1,'importConFusionData.m']]],
  ['batches',['batches',['../importConFusionData_8m.html#ae4e917d48fb6323948e6b6cd2a4c0b63',1,'importConFusionData.m']]],
  ['batchsize_5f',['batchSize_',['../classconfusion_1_1TagTracker.html#ace98bb8a73422d75665e6556892b236c',1,'confusion::TagTracker']]],
  ['bg_5finit_5fstddev',['bg_init_stddev',['../structTagTrackerParameters.html#a03c28e529faf17648927330802ee09d6',1,'TagTrackerParameters']]],
  ['bg_5fstddev_5f',['bg_stddev_',['../structTagTrackerParameters.html#a41058d34055fafb6af509fea91e74bfa',1,'TagTrackerParameters::bg_stddev_()'],['../ImuChainTest_8cpp.html#a4aa453615ec96551d8ce55b105cf29cc',1,'bg_stddev_():&#160;ImuChainTest.cpp']]],
  ['bits',['bits',['../structbit__decoder.html#abc7dbc9aea08b0e78415f65adee7ded2',1,'bit_decoder']]],
  ['black_5fborder',['black_border',['../structapriltag__family.html#a83bea52c56717c7d705f15f1b5b81dd1',1,'apriltag_family']]],
  ['bstar_5f',['bstar_',['../classconfusion_1_1PriorConstraint.html#a813d0fbcf796c489493b7f032dfbe2dc',1,'confusion::PriorConstraint']]],
  ['buf',['buf',['../structimage__u8.html#a1225519e3da4d9c8840081cdd76ee810',1,'image_u8::buf()'],['../structimage__u8x3.html#a915b904d3189f89e8c28c3fc0872c8f5',1,'image_u8x3::buf()'],['../structimage__u8x4.html#afcfe4d4204b158c7a854df3bd606d8a6',1,'image_u8x4::buf()'],['../structimage__f32.html#a38bbd7f68b4aa871164ccaafcf1234ae',1,'image_f32::buf()'],['../structimage__u32.html#ad2c56a74774f7a48634a2b0bd8cda40e',1,'image_u32::buf()'],['../structpnm.html#a52e9130ec5a276266eafb7fcf2d9aa53',1,'pnm::buf()']]],
  ['buflen',['buflen',['../structpnm.html#a603d1074f8d897879ea2c188daed37fc',1,'pnm']]]
];
