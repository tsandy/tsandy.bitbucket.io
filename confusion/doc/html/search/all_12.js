var searchData=
[
  ['randomwalkprocess_5f',['randomWalkProcess_',['../classconfusion_1_1Parameter.html#a5e83097f1c6c4435ecca82fd89f5f9d4',1,'confusion::Parameter']]],
  ['randomwalkprocessactive_5f',['randomWalkProcessActive_',['../classconfusion_1_1Parameter.html#a05bcc9c6160b0b67f0184291dcb3039f',1,'confusion::Parameter']]],
  ['rcode',['rcode',['../structquick__decode__entry.html#a6633de79b7cb0f0e4e2d52df6189fb34',1,'quick_decode_entry::rcode()'],['../structevaluate__quad__ret.html#ac7d9f2926a83724b2e989fc376738a81',1,'evaluate_quad_ret::rcode()']]],
  ['readlogfileline_2em',['readLogFileLine.m',['../readLogFileLine_8m.html',1,'']]],
  ['readme_2emd',['README.md',['../README_8md.html',1,'']]],
  ['readtagposes',['readTagPoses',['../TagTrackerParameters_8h.html#aea5ab44792af3656bd65f0856e81dc1b',1,'TagTrackerParameters.h']]],
  ['ready',['ready',['../classconfusion_1_1ProcessChain.html#a40521d0e4b0b01ee1f12d1d4eb5a610d',1,'confusion::ProcessChain']]],
  ['ready_5f',['ready_',['../classconfusion_1_1ProcessChain.html#a185edabedf2774a06bbe809e91b22031',1,'confusion::ProcessChain']]],
  ['refdroptime',['refDropTime',['../classconfusion_1_1TrajectoryAlignmentCost.html#a12eb6171707b25ce80be6acde60beaf3',1,'confusion::TrajectoryAlignmentCost']]],
  ['referenceframename',['referenceFrameName',['../classconfusion_1_1PoseMeas.html#ab3d55261aafd9b8cb8027995f217c1e0',1,'confusion::PoseMeas']]],
  ['referenceframename_5f',['referenceFrameName_',['../classconfusion_1_1PoseMeas.html#ac0776587036bd8ca8098673b60071c3a',1,'confusion::PoseMeas::referenceFrameName_()'],['../classconfusion_1_1TagMeas.html#afdc2878b5b31dea44d8fbe2ce3de3643',1,'confusion::TagMeas::referenceFrameName_()']]],
  ['referenceframeoffsets_5f',['referenceFrameOffsets_',['../classImuState.html#ac2ab228fa768d430ffc7d342f7d99e20',1,'ImuState::referenceFrameOffsets_()'],['../classconfusion_1_1TagTracker.html#a8261825efb906150802f6b99c96c4280',1,'confusion::TagTracker::referenceFrameOffsets_()']]],
  ['referencetrajectory',['referenceTrajectory',['../classconfusion_1_1TrajectoryAlignmentCost.html#a565986b18e9d09c1135f86b2ed5a1fbf',1,'confusion::TrajectoryAlignmentCost']]],
  ['refine_5fdecode',['refine_decode',['../structapriltag__detector.html#af17e618c777bf28d769e15f22d8af34e',1,'apriltag_detector']]],
  ['refine_5fedges',['refine_edges',['../structapriltag__detector.html#a7f28ffdc2f9a642549820c509ea478a9',1,'apriltag_detector']]],
  ['refine_5fpose',['refine_pose',['../structapriltag__detector.html#a4c6aaf83454ae956df95efb2224a381e',1,'apriltag_detector']]],
  ['remove_5fvertex',['remove_vertex',['../structremove__vertex.html',1,'']]],
  ['removefrontupdatemeasurement',['removeFrontUpdateMeasurement',['../classconfusion_1_1MeasurementManager.html#ab5aeed9c3e408aa25a26faff0dbd09d1',1,'confusion::MeasurementManager']]],
  ['removeintermediatestate',['removeIntermediateState',['../classconfusion_1_1ConFusor.html#aff4a55d444b7710f75c75e63f2b455a7',1,'confusion::ConFusor']]],
  ['removeparameter',['removeParameter',['../classconfusion_1_1StaticParameterVector.html#ae4588a540efdafcbaf762fda123536c2',1,'confusion::StaticParameterVector']]],
  ['removestate',['removeState',['../classconfusion_1_1StateVector.html#acdaae5357f7ca4b9b98dc7f131b99091',1,'confusion::StateVector']]],
  ['removestaticparameter',['removeStaticParameter',['../classconfusion_1_1PriorConstraint.html#aa3cfa213aedad0602d299b3cf1c3ecb4',1,'confusion::PriorConstraint']]],
  ['removestaticparameters',['removeStaticParameters',['../classconfusion_1_1ConFusor.html#a4c6906ce4bb5afbbec438db96c725c91',1,'confusion::ConFusor']]],
  ['reordernormalequations',['reorderNormalEquations',['../namespaceconfusion.html#a0d86244c9d364b7024ab41e9bf68d010',1,'confusion']]],
  ['reset',['reset',['../classconfusion_1_1BatchFusor.html#a937924feb51deffdbde1d61ae3c5b280',1,'confusion::BatchFusor::reset()'],['../classconfusion_1_1MeasurementManager.html#a82cf6cf8a946661c75e118f9f223341c',1,'confusion::MeasurementManager::reset()'],['../classconfusion_1_1PriorConstraint.html#a30998728691be354e0cd9e638c21a220',1,'confusion::PriorConstraint::reset()'],['../classconfusion_1_1ProcessChain.html#a4c75bf632ca63ed3f75743e49676aab9',1,'confusion::ProcessChain::reset()'],['../classconfusion_1_1State.html#a964899dd188991a46d0213fd20f28aa2',1,'confusion::State::reset()'],['../classconfusion_1_1StateVector.html#a95eaa7dc260d1cc2bf56500e14f23a6f',1,'confusion::StateVector::reset()']]],
  ['reset_5fcount',['reset_count',['../structpjpeg__decode__state.html#a67ee82f0076f9200b03342e4d74f6376',1,'pjpeg_decode_state']]],
  ['reset_5finterval',['reset_interval',['../structpjpeg__decode__state.html#ab30d7bbb9bd8d7b47ffb5c536abc6332',1,'pjpeg_decode_state']]],
  ['reset_5fnext',['reset_next',['../structpjpeg__decode__state.html#ab076ddd4819494dbe10e896c7e1872df',1,'pjpeg_decode_state']]],
  ['residualblockid',['residualBlockId',['../classconfusion_1_1ProcessChain.html#a2c2d0b3b7df0992edf55f288accfa7eb',1,'confusion::ProcessChain::residualBlockId()'],['../classconfusion_1_1UpdateMeasurement.html#ac3701fac2589ea1f3039db00d25d68ba',1,'confusion::UpdateMeasurement::residualBlockId()']]],
  ['residualblockid_5f',['residualBlockId_',['../classconfusion_1_1ProcessChain.html#a45d38f98f55a367261ef6829a0a2c005',1,'confusion::ProcessChain::residualBlockId_()'],['../classconfusion_1_1UpdateMeasurement.html#a0e7f905a8a1d4a46453322fc3301ed82',1,'confusion::UpdateMeasurement::residualBlockId_()']]],
  ['residualdimension',['residualDimension',['../classconfusion_1_1ImuChain.html#ab8169c1957e7b2bbba6e6b6039caf900',1,'confusion::ImuChain::residualDimension()'],['../classconfusion_1_1PoseMeas.html#a9534a9f89ebdd8d7f03b78f5780348c8',1,'confusion::PoseMeas::residualDimension()'],['../classconfusion_1_1TagMeas.html#ae0410afc7b5bf231841f71843b342e3f',1,'confusion::TagMeas::residualDimension()'],['../classconfusion_1_1ProcessChain.html#a0180896d541028d84a1b59ef9f5f3f97',1,'confusion::ProcessChain::residualDimension()'],['../classconfusion_1_1State.html#a68bbc616886d018c0623f7833b7aba5d',1,'confusion::State::residualDimension()'],['../classconfusion_1_1UpdateMeasurement.html#abe4612fbc0c8df5a0d5403ad9f163c91',1,'confusion::UpdateMeasurement::residualDimension()'],['../classTestUpdateMeas.html#aaefc07676ff82506c0302e4ebc8475f0',1,'TestUpdateMeas::residualDimension()'],['../classTestProcessChain.html#ad7f8f0c06a226566d3285096a68195e6',1,'TestProcessChain::residualDimension()'],['../classTestUpdateMeas.html#aaefc07676ff82506c0302e4ebc8475f0',1,'TestUpdateMeas::residualDimension()'],['../classTestProcessChain.html#ad7f8f0c06a226566d3285096a68195e6',1,'TestProcessChain::residualDimension()']]],
  ['right',['right',['../structremove__vertex.html#a7213256f85fc7aeba3250d47e94c644d',1,'remove_vertex::right()'],['../structsegment.html#a8f41183e65f4302f347f0013f042a931',1,'segment::right()']]],
  ['rigidbodyvelocitytransform',['rigidBodyVelocityTransform',['../namespaceconfusion.html#a2f9367b29354de3a0a7b3a83c754b823',1,'confusion']]],
  ['ros_5fconversions_2eh',['ros_conversions.h',['../ros__conversions_8h.html',1,'']]],
  ['rosmsgtoconfusionimumeas',['rosmsgToConfusionImuMeas',['../namespaceconfusion.html#ac9d6dd2d3363ac228d6f194f32005de5',1,'confusion']]],
  ['rosmsgtoquaternion',['RosMsgToQuaternion',['../namespaceconfusion.html#a24d1418c18ca23d4216e4aaaa19b8500',1,'confusion']]],
  ['rosmsgtovector',['RosMsgToVector',['../namespaceconfusion.html#a32c64423fe88d63d6107452ff5e330eb',1,'confusion']]],
  ['rot',['rot',['../classconfusion_1_1Pose.html#a3989aa3404fcf71ea1ca5816c2a79557',1,'confusion::Pose']]],
  ['rotatevelocityvector',['rotateVelocityVector',['../classconfusion_1_1Pose.html#a89292bf6da7b7ce7b30b259fed80786b',1,'confusion::Pose']]],
  ['rotation',['rotation',['../structquick__decode__entry.html#a78c02d66a3be0db1c5a5132a38cb94aa',1,'quick_decode_entry']]],
  ['rotation_5futils_2eh',['rotation_utils.h',['../rotation__utils_8h.html',1,'']]],
  ['rows',['rows',['../plotConFusionData_8m.html#a74742cb5c8e2ac354a60cb73383d8176',1,'plotConFusionData.m']]],
  ['rpy_5fto_5fquat',['rpy_to_quat',['../namespaceconfusion.html#a584d3cbb7958d8f77ab1341e64e908f6',1,'confusion::rpy_to_quat(double roll, double pitch, double yaw)'],['../namespaceconfusion.html#aa85e2f149c5169200f36d57c17b32e45',1,'confusion::rpy_to_quat(Eigen::Vector3d rpy)']]],
  ['run',['run',['../classconfusion_1_1Diagram.html#a451b6744e80ea90b44039de3d379e39c',1,'confusion::Diagram']]],
  ['run_5f',['run_',['../classconfusion_1_1TagTracker.html#a4153c22b382e71d2c66272e0db4f54f1',1,'confusion::TagTracker']]],
  ['runbatch_5f',['runBatch_',['../classconfusion_1_1TagTracker.html#a4e25dfaa42a20be9924559dd3380dd27',1,'confusion::TagTracker']]],
  ['runconfusor',['runConFusor',['../ConFusorTest_8cpp.html#ab6a761b812fc4d6cbe418ef6715b76db',1,'ConFusorTest.cpp']]],
  ['runestimator',['runEstimator',['../classconfusion_1_1TagTracker.html#a74c04480ce774b0f69ed2f62e9203fff',1,'confusion::TagTracker']]],
  ['runestimatorloopdone_5f',['runEstimatorLoopDone_',['../classconfusion_1_1TagTracker.html#aa05f3e1da355a4a8614099ec9884a315',1,'confusion::TagTracker']]],
  ['rwppriorsideparameteraddress',['rwpPriorSideParameterAddress',['../classconfusion_1_1Parameter.html#aca2f5b834c1b5b96d6b907cb8c6daf4d',1,'confusion::Parameter']]],
  ['rwppriorsideparametervalue_5f',['rwpPriorSideParameterValue_',['../classconfusion_1_1Parameter.html#af76c8b1fa8bde9696ec2ed30f062bd77',1,'confusion::Parameter']]]
];
